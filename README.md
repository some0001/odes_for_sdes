# **Deterministic particle dynamics for simulating probability flows of stochastic systems**

 

( for detailed info please read the [relevant article](https://www.mdpi.com/1099-4300/22/8/802/htm))

(if math equations do not render properly click [here](https://gitlab.com/dimitra-maoutsa/odes_for_sdes/-/blob/master/README.md) )

(  The code to produce the plots for the 1D systems is provided [here](https://gitlab.com/dimitra-maoutsa/odes_for_sdes/-/blob/master/notebooks/Make_1D_animation_Fokker_Planck.ipynb) in the folder "_notebooks_".  
 A package with the whole framework for more than one dimensions as in the paper is getting cleaned up and will be uploaded soon. )






**Particle-based** framework for simulating **solutions of Fokker–Planck equations** that
- is **effortless** to set up
- provides **smooth transient solutions**
- is **computationally efficient**
- does not involve **space discretisation**.

## Introduction
 Consider a stochastic system following the **SDE** (stochastic differential equation) 
```math
dX_t= f(X_t) dt  + \sigma dW_t. \quad \quad \quad (1)
```

The **Fokker-Planck equation** (FPE) describes the temporal evolution of the probability density of the system state

```math
\frac{\partial p_t(x)}{\partial t} =  -\nabla\cdot \left[ f(x) p_t(x)  - \frac{\sigma^2}{2} \nabla p_t(x)\right].\quad \quad \quad (2)
```

To simulate solutions of the Fokker-Planck equation in settings where an analytical solution is intractable, we often have to either resort to _stochastic simulations_ of Eq.(1), or employ _PDE solvers_ to treat Eq.(2) directly.

- **Stochastic simulations** are rather **trivial to set up**, involving thereby cheap calculations per single trajectory/realisation. However, often, a rather **large sample number** is required to attain accurate solutions, which in turn means both **increased memory and computational demands**.

- On the other hand, **PDE solvers** provide **smooth transient solutions**, but involve **space discretisation**, are rather computationally demanding in large dimensions, and are **not** always **straightforward** to set up.
    
The **deterministic particle framework** proposed here provides a solution that combines, in our view, the **best of both worlds!**    


## A. **From SDEs to ODEs**
- ### Systems with additive noise
    

    We may rewrite the FPE in the form of a **_Liouville equation_** $`\textcolor{teal}{\left[ \text{Eq.(3-5) in the main text}\right]  } `$
    ```math
    \frac{\partial p_t(x)}{\partial t} =  -\nabla\cdot \left[{\textcolor{maroon}{\left(f(x) - \frac{\sigma^2}{2} \nabla \ln p_t(x)\right)}}\; p_t(x) \right],  \quad \quad \quad (3)
    ```

    that we may, in turn, interpret as the evolution equation of the probability distribution of a statistical ensemble of $`N`$ **_deterministic_** dynamical systems following the **ODE** $`\textcolor{teal}{\left[Eq.(4-5) \text{in the main text}\right]  } `$
    ```math
    \frac{dX_t^{(i)}}{dt} = \textcolor{maroon}{f(X_t^{(i)})) - \frac{\sigma^2}{2} \nabla \ln p_t(X_t^{(i)})}, \quad \quad \quad (4)
    ```
    with $`i=1,\dots,N`$.

- ### Systems with multiplicative noise
    In a similar vain, for a **_state dependent_** diffusion 
    ```math
    dX_t= f(X_t) dt  + \sigma(X_t) dW_t,
    ```
    the associated deterministic particle dynamics are 
    ```math
    \frac{dX_t^{(i)}}{dt} = \textcolor{maroon}{f(X_t^{(i)}) - \frac{\sigma(X_t^{(i)})\sigma(X_t^{(i)})^{\intercal}}{2} \nabla \ln p_t(X_t^{(i)}) - \frac{1}{2} \nabla \cdot \sigma(X_t^{(i)})\sigma(X_t^{(i)})^{\intercal}   },
    ```
    which, by setting $`D(x) = \sigma(x) \sigma(x)^{\intercal},`$ become $`\textcolor{teal}{\left[\text{Eq.(53)  in the main text}\right]  }  `$

    ```math
    \frac{dX_t^{(i)}}{dt} = \textcolor{maroon}{f(X_t^{(i)}) - \frac{D(X_t^{(i)})}{2} \nabla \ln p_t(X_t^{(i)}) - \frac{1}{2} \nabla \cdot D(X_t^{(i)})   }. \quad \quad \quad (5)
    ```




Eq.(4) and Eq.(5) imply that we may obtain transient solutions of the associated FPEs by simulating ensembles of **deterministic** trajectories/particles with initial conditions drawn from the starting distribution $`p_0(x)`$. 

**However, the deterministic particle dynamics in Eq.(4) and Eq.(5) require the knowledge of $`\nabla_x \ln p_t(x),`$ i.e. the gradient of the logarithm of the quantity of interest.**   
So are we just making circles?

**Here enters the gradient-log density estimator (score function estimator) that provides direct estimations of the $`\nabla_x \ln p_t(x)`$ without any requirement to compute normalising factors that a direct approximation of $` p_t(x)`$ would involve.**



## B. **Gradient-log-density (score function) estimator**

- ### _Hyv&auml;rinen score matching_ 

    Assume samples $`\{X_i\}_{i=1}^{N} \in \mathrm{R}^d`$ from some _unknown_ prob. distribution $`\mathcal{P}`$ on $`\mathrm{R}^d`$, approximate $`\mathcal{P}`$ by some distribution $`\mathcal{Q}`$ from a specified family of distributions $`\mathrm{D}`$ by minimising (see [2] for derivation)
    ```math

    \mathcal{L}[\mathcal{Q},\mathcal{P}] = \frac{1}{2} \int dx \,p(x) \| \nabla_x \ln p(x) - \nabla_x \ln q(x)  \|^2 \\
    \quad \quad \quad \quad \quad \quad \; \;  = \int dx \,p(x) [ \Delta_x \ln q(x) + \frac{1}{2} \| \nabla_x \ln q(x)  \|^2  ] + \mathrm{C},  \quad (6)
    ```

    with $`C`$ a constant irrelevant to the minimisation.

    By minimising the objective $`\mathcal{L}[\mathcal{Q},\mathcal{P}]`$ within the family $`\mathrm{D}`$ we are able to approximate $`p(x)`$ without any need to compute intractable partition functions. Indeed, the optimisation objective involves only expectations of terms with the **model distribution $`q`$** over the **true distribution** $`p`$ from which we have samples.

- ### _Variational formulation of the gradient-log-density (score) estimator_

    Here we employ a **variational representation** of the gradient-log-density where we estimate the score function component-wise as
    ```math
    {\partial_\alpha} \ln p(x) = 
    \arg\min_\phi\; {\mathcal{L}}_\alpha [{\phi}, p]{(x)},
    ```
    where $`\alpha`$ denotes the dimension along which the gradient estimation is performed. The optimisation objective is inspired by the Hyvarinen objective in Eq.(6). 

    ```math
    {\mathcal{L}}_\alpha [\phi,p] = \int p(x) \left(\phi^2(x) + 2 \partial_\alpha \phi(x)\right) dx  
    ```

- ### _Kernel-based gradient-log-density (score) estimation_

    We regularise the problem through RKHS $`\mathcal{F}`$ associated with kernel $`K`$

    ```math
    \partial_\alpha \ln p(x) \approx  
    \arg\min_{\phi\in {\mathcal{F}}}\; \left\{{\mathcal{L}}_\alpha[\phi, \hat{p}] +  \frac{\lambda}{N} \| \phi \|_{{\tiny{RKHS}}}^2\right\}{(x)} 
    ```
    
    
    From Representer theorem the function $`\phi(\cdot)`$ may be expressed as
    
    ```math 
    \phi(x) = \sum_{i=1}^N a_i K(x,X_i) .
    ```
    
    
    
    Therefore the minimising solution may be computed by solving for
    ```math
    a_j = - \sum_{k=1}^N \textcolor{orange}{\left((K^2 + \lambda K)^{-1}\right)_{jk} } \sum_{l=1}^N \left\{\partial_{\alpha_l} K(X_l, X_k) 
    \right\}
    ```

    Yet, this calculation entails an expensive matrix inversion of the order of $`\textcolor{orange}{ \mathcal{O}(N^3)}`$.

- ### _Sparse kernel approximation of gradient-log-density (score) estimator_

    We reduce the high computational demands by introducing $`M\ll N`$  **_inducing points_** $`\{z_k\}_{k=1}^M`$.

    We now minimise the cost function 
    ```math
    \partial_\alpha \ln p(x) \approx  
    \arg\min_{\phi\in {\mathcal{F}}}\; \left\{{\mathcal{L}}_\alpha[\phi, \hat{p}] +  \frac{\lambda}{N} \| \phi \|_{{\tiny{RKHS}}}^2\right\}{(x)} 
    ```
    in the family of functions 
    ```math
    \phi(x) = \sum_{i=1}^M a_i  K(x,z_i)   .
    ```

    The resulting estimator has lower computational complexity, and the minimising solution may be computed as
    ```math
    A \doteq K^{xz} \left[ \lambda  I + 
    (K^{zz})^{-1} (K^{xz})^\top (K^{xz})\right]^{-1}(K^{zz})^{-1} , \quad \quad \quad (7)
    ```
    ```math
    \alpha_i = \sum_k A_{i k}\sum_l 
    \left\{\nabla_l K(X_l, z_k) \right\}
    ```

    
Finally, we formulate the resulting Fokker--Planck particle dynamics as
```math 
\frac{dX_i}{dt} = f(X_i)  +  
\frac{\sigma^2}{2} \sum_k A_{i k}\sum_l 
\left\{\nabla_l K(X_l, z_k)\right\} ,
```
where $`A_{i k}`$ the element of $`A`$ in $`i`$-th row and $`k`$-th column from Eq.(7). 

## C. **Smooth transient solutions of Fokker-Planck equations**

Evolution of Fokker-Planck density for a one dimensional bistable system resulting from simulating ensembles of
$`S_{\infty}=80000`$ (_grey_) and $`S=200`$ (_magenta_) particles following **stochastic** dynamics, and an ensemble of $`D=200`$ (_light blue_) particles with **deterministic** dynamics (employing M=50 inducing points in the sparse kernel approximation). 

_Left column_: particle trajectories for the stochastic and deterministic particle systems.  
_Middle column_: instantaneous densities for the three systems.  
_Right column_: evolution of the first four central moments for the three particle ensembles.  

[ **The code to produce these plots is provided [here](https://gitlab.com/dimitra-maoutsa/odes_for_sdes/-/blob/master/notebooks/Make_1D_animation_Fokker_Planck.ipynb) in the folder notebooks** ]

<img src="images/Fokker_plot.png"  width="90%" height="90%">

<img src="images/high_speed.gif"  width="90%" height="90%">

<img src="images/OU2Dc.png"  width="40%" height="40%">


<img src="images/Linear_Ham.png"  width="50%" height="50%">



## D. **Mathematical Background**

## E. **Convergence Properties**

**Citations:**

1. Maoutsa, Dimitra; Reich, Sebastian; Opper, Manfred. [**Interacting Particle Solutions of Fokker–Planck Equations Through Gradient–Log–Density Estimation.**](https://www.mdpi.com/1099-4300/22/8/802/htm) _Entropy_ 2020, 22, 802. 

2. Hyvärinen, Aapo. **Estimation of non-normalized statistical models by score matching.** _Journal of Machine Learning Research_ 2005, 695-709.

